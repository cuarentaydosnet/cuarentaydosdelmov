const argon2 = require('argon2');
const requestJson = require('request-json');
const uuid = require('uuid/v4');

exports.handler = function(event,context,callback){
    console.log("POST movimientos");
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    
    let body = JSON.parse(event.body);

        var httpClient = requestJson.createClient(db);
        var query="account?q={\"_id\": \""+ event.pathParameters.accountid + "\"}&" + apikey
        console.log(query)
        httpClient.get(query, function(err,resMLab,cuenta){
             if(err){
                 console.log("error feo")
             } else {
                var micuenta = cuenta[0]
                var movimientos = micuenta.movimientos
                var removeIndex = movimientos.map(function(item) { return item.id; }).indexOf(event.pathParameters.movimientoid);
                movimientos.splice(removeIndex, 1);

                 var newdocument = '{ "$set" :{ "movimientos" : ' + JSON.stringify(movimientos) + ' }}';
                 console.log(newdocument + typeof newdocument)
                 httpClient.put("account/" + micuenta._id+ "?" + apikey, JSON.parse(newdocument), function(err,resMLab,body){
                     if(err){
                         var response = {
                             "statusCode" : 500,
                             "body": JSON.stringify(err),
                         }
                     } else {
                         var response = { 
                            "statusCode": 200,
                            "headers" : {
                              "Access-Control-Allow-Origin": "*"
                            },
                            "body": JSON.stringify(body),
                            "isBase64Encoded": false
                     };
                     }
                     callback(null,response);
                 })
             } 
         })
}
